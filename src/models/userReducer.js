export default function (state = [], action={}) {
    switch (action.type) {
    case "GET_PROFILE_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "GET_PROFILE_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            data: action.payload.data,
            isError: false,
            message:"Get Profile Successfully",
        };
    case "GET_PROFILE_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };
    


    case "UPDATE_PROFILE_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "UPDATE_PROFILE_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            isError: false,
            message:"Update Post Successfully",
        };
    case "UPDATE_PROFILE_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };


    case "SEARCH_PROFILE_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "SEARCH_PROFILE_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            isError: false,
            message:"Update Post Successfully",
        };
    case "SEARCH_PROFILE_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };   

    default:
        return { 
            ...state
        };
    }
}