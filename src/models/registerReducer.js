export default function (state = [], action={}) {
    switch (action.type) {
    case "REGISTRATION_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "REGISTRATION_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            isError: false,
            message:"Registration Successfully",
        };
    case "REGISTRATION_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };
    
    default:
        return { 
            ...state
        };
    }
}