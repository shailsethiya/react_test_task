export default function (state = [], action={}) {
    switch (action.type) {
    case "GET_POST_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "GET_POST_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            isError: false,
            message:"Post Successfully",
        };
    case "GET_POST_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };



    case "CREATE_POST_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "CREATE_POST_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            isError: false,
            message:"Create Successfully",
        };
    case "CREATE_POST_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };



    case "UPDATE_POST_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "UPDATE_POST_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            isError: false,
            message:"Update Successfully",
        }; 
    case "UPDATE_POST_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };



    case "DELETE_POST_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false,
        };
    case "DELETE_POST_SUCCESS":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            isError: false,
            message:"Delete Successfully",
        };
    case "DELETE_POST_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message,
        };

    default:
        return { 
            ...state
        };
    }
}