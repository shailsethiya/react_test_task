export default function (state = [], action={}) {
    switch (action.type) {
    case "LOGIN_LOADING":
        return {
            ...state,
            isLoading: true,
            isSuccess: false,
            isError: false
        };
    case "LOGIN_SUCCESS":
        return {
            ...state,
            isLoading: false,
            user: action.payload.response,
            isSuccess: true,
            isError: false
        };
    
    case "LOGIN_ERROR":
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: true,
            message:action.payload.response.data.message
        };

    case "GET_USER":
        return {
            ...state,
            isLoading: false,
            isSuccess: true,
            userData: action.payload,
            error: false
        };  
    
    case "LOGOUT":
        window.localStorage.clear();
        location.reload();
        return {
            ...state,
            isLoading: false,
            isSuccess: false,
            isError: null
        };
    
    default:
        return { 
            ...state
        };
    }
}