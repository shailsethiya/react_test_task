import React, {Component} from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Header from "../../components/Header/";
import Footer from "../../components/Footer/";
import { Row } from "reactstrap";
import Logout from "../../components/Logout/";
import Dashboard from "../../views/Dashboard/";
import AddPost from "../../views/Post/AddPost/";
import UpdatePost from "../../views/Post/UpdatePost/";
import ChangePassword from "../../views/ChangePassword/";
import LeftSideBar from "../../components/LeftSideBar/";
import RightSideBar from "../../components/RightSideBar/";
import UpdateProfile from "../../views/UpdateProfile";
import ProfileDetails from "../../views/ProfileDetails";
import {NotificationContainer} from "react-notifications";
import Proptypes from "prop-types";


export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem("user_info")
            ? <Component {...props} />
            : <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
    )} />
);

class Full extends Component {
    render() {
        return (
            <div className="app">
                <NotificationContainer/>
                <Header/>
                <Row>
                    <LeftSideBar />
                    <div className="app-body">
                        <Switch>
                            <PrivateRoute path="/logout" name="Logout" component={Logout} />
                            <PrivateRoute path="/addpost" name="AddPost" component={AddPost} />
                            <PrivateRoute path="/updatepost/:id" name="UpdatePost" component={UpdatePost} />
                            <PrivateRoute path="/dashboard" name="Dashboard" component={Dashboard} />
                            <PrivateRoute path="/changepassword" name="ChangePassword" component={ChangePassword} />
                            <PrivateRoute path="/updateprofile" name="UpdateProfile" component={UpdateProfile} />
                            <PrivateRoute path="/profiledetails/:id" name="ProfileDetails" component={ProfileDetails} />
                        </Switch>
                    </div>
                    <RightSideBar />
                </Row>
                <Footer/>
            </div>
        );
    }
}

PrivateRoute.propTypes = {
    component: Proptypes.func,
    location: Proptypes.object,
};


export default Full;