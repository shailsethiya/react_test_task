import React from "react";
import ReactDOM from "react-dom";
import {HashRouter, Switch, Redirect} from "react-router-dom";
import "react-notifications/lib/notifications.css";
import { Route,browserHistory } from "react-router";
import {Provider} from "react-redux";
import Proptypes from "prop-types";
import "font-awesome/css/font-awesome.min.css";
import "simple-line-icons/css/simple-line-icons.css";
import "../scss/style.scss";
import "../scss/core/_dropdown-menu-right.scss";
import Full from "./containers/Full/";
import Login from "./views/Pages/Login/";
import Page404 from "./views/Pages/Page404/";
import Page500 from "./views/Pages/Page500/";
import Register from "./views/Pages/Register/";
import configureStore from "./store";

const store = configureStore();

function loggedIn() {
    if(JSON.parse(localStorage.getItem("auth_token")).token !== null 
      || JSON.parse(localStorage.getItem("auth_token")).token !== undefined)
    {
        return true;
    }
    return false;
}


function requireAuth() {
    if (!loggedIn()) {
        return true;
    }
    else {
        return false;
    }
}

function PrivateRoute ({component: Component, ...rest}) {
    const user_info = window.localStorage.getItem("user_info") ? JSON.parse(window.localStorage.getItem("user_info")) : null;
  
    return (
        <Route
            {...rest}
            render={(props) => user_info
                ? <Component {...props} />
                : <Redirect to={{pathname: "/login", state: {from: props.location}}} />}
        />
    );
}

ReactDOM.render((
    <Provider store={store} history={browserHistory}>
        <HashRouter>
            <Switch>
                <Route exact path="/login" name="Login Page" render={() => (
                    window.localStorage.getItem("user_info")? (
                        <Redirect to="/dashboard"/>
                    ) : (
                        <Login/>
                    )
                )}/>
                <Route onEnter={requireAuth} exact path="/login" name="Login Page" component={Login}/>
                <Route exact path="/404" name="Page 404" component={Page404}/>
                <Route exact path="/500" name="Page 500" component={Page500}/>
                <Route exact path="/register" name="Register Page" component={Register} />
                <PrivateRoute authed={requireAuth} path="/" name="Home" component={Full} />
            </Switch>
        </HashRouter>
    </Provider>
), document.getElementById("root"));

PrivateRoute.propTypes = {
    component: Proptypes.func,
    authed: Proptypes.func,
    location: Proptypes.object,
};