import React, {Component} from "react";
import { Link } from "react-router-dom";
import Proptypes from "prop-types";
import { getProfile } from "../../actions/actionUser";
import { CardHeader, Card, Col, CardBody, ListGroupItem, ListGroup } from "reactstrap";
import { connect } from "react-redux";

class LeftSideBar extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentDat : null,
            id: 12
        };
    }

    componentDidMount(){
        this.props.getProfile().then((response)=>{
            this.setState({currentDat: response.value.data.find(dat => dat.id == this.state.id)});
        });
    }
  
    render(){
        const {currentDat} = this.state;
        return(
            <div className="leftsidebarsyle">
                <Col>
                    {currentDat ?  
                        <Card>
                            <CardBody>
                                <center><img src={currentDat.pic} style={{"width": "100px"}} alt="Profile" /></center>
                                <h3 className="textfont"> <center>{currentDat.first_name}</center> </h3> 
                            </CardBody>
                        </Card> :  " " }
                    <Card>
                        <CardHeader>Account Settings</CardHeader>
                        <CardBody>
                            <ListGroup id="list-tab" role="tablist">
                                <ListGroupItem action color="primary"> <Link to="/dashboard"><div style={{"color" : "#336573"}}> My Posts </div> </Link> </ListGroupItem>
                                <ListGroupItem action color="primary"> <Link to="/addpost"><div style={{"color" : "#336573"}}> Add Posts </div> </Link> </ListGroupItem>
                                <ListGroupItem action color="primary"> <Link to="/updateprofile"><div style={{"color" : "#336573"}}> Profile Details</div> </Link> </ListGroupItem>
                                <ListGroupItem action color="primary"> <Link to="/changepassword"><div style={{"color" : "#336573"}}> Change Password </div> </Link> </ListGroupItem>
                            </ListGroup>     
                        </CardBody>
                    </Card>
                </Col>
            </div>
        );
    }
}


LeftSideBar.propTypes = {
    loginUser: Proptypes.object,
    getProfile: Proptypes.func
};

function mapStateToProps(state) {
    return {
        isError: state.post.isError,
        data: state.post.data || []
    };
}

function mapDispatchToProps(dispatch) {
    return({
        getProfile: () => dispatch(getProfile())
    });
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LeftSideBar);