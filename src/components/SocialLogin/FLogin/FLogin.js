import React, {Component} from "react";
import FacebookLogin from "react-facebook-login";

// const responseFacebook = (response) => {
//     console.log("response", response);
// };

class FLogin extends Component{
    render() {
        return (
            <FacebookLogin
                appId="1088597931155576"
                autoLoad={true}
                className="kep-login-facebook"
                fields="name,email,picture"
                // callback={responseFacebook} 
            /> 
        );
    }
}

export default FLogin;
