import React, {Component} from "react";
import GoogleLogin from "react-google-login";

// const responseGoogle = (response) => {
//     console.log("response", response);
// };

class GLogin extends Component{
    render() {
        return (
            <GoogleLogin
                clientId="555524057821-evfg7jgb5baj6v5ian4bna3rlani5kc2.apps.googleusercontent.com"
                buttonText="Google+"
                autoLoad={true}
                disabled={true}
                className="googlebtn"
                // onSuccess={responseGoogle}
                // onFailure={responseGoogle}
            />
        );
    }
}

export default GLogin;
