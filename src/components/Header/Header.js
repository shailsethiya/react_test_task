import React, {Component} from "react";
import { NavbarToggler } from "reactstrap";
import { connect } from "react-redux";
import { logout } from "../../actions/actionLogin";
import { Button } from "reactstrap";
import { getProfile } from "../../actions/actionUser";
import Proptypes from "prop-types";


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentProf : [],
            id : 12
        };
        this.confirmLogout = this.confirmLogout.bind(this);
    }

    componentDidMount(){
        this.props.getProfile().then((response)=>{
            this.setState({currentProf: response.value.data.find(dat => dat.id == this.state.id)});
        });
    }

    confirmLogout(){
        this.props.logout();
    }

    sidebarMinimize(e) {
        e.preventDefault();
        document.body.classList.toggle("sidebar-minimized");
    }

    mobileSidebarToggle(e) {
        e.preventDefault();
        document.body.classList.toggle("sidebar-mobile-show");
    }

    asideToggle(e) {
        e.preventDefault();
        e.target.classList.add("fa-angle-left");
        document.body.classList.toggle("aside-menu-hidden");
    }

    render() {
        const {currentProf} = this.state;
        return (
            <header className="app-header navbar">
                <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
                    <span className="navbar-toggler-icon"></span>
                </NavbarToggler>
                <h5 className="header-text">{currentProf.first_name}</h5>
                <div className="flex_block">
                </div>
                <Button color="dark" onClick={this.confirmLogout}  style= {{ "marginRight":"45px", "borderRadius": "5px"}}> <i className="fa fa-sign-out"></i> Logout</Button>
            </header> 
        );
    }
}

Header.propTypes = {
    logout: Proptypes.func,
    getProfile: Proptypes.func
};

function mapStateToProps(state) { 
    return {
        isError: state.post.isError,
        data: state.post.data || []
    };
}

function mapDispatchToProps(dispatch) {
    return({
        logout: () => dispatch(logout()),
        getProfile: () => dispatch(getProfile())
    });
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);