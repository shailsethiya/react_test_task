import React, {Component} from "react";
import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Dropdown
} from "reactstrap";
import Proptypes from "prop-types";

class HeaderDropdown extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false
        };
        this.logOut = this.logOut.bind(this);
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    logOut(){
        this.props.logout();  
    }


    dropAccnt() {
        return (
            <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle nav>
                    <span className="navbar-toggler-icon"></span>
                </DropdownToggle>
                <DropdownMenu right>
                    <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
                    <DropdownItem onClick={this.logOut}><i className="fa fa-lock"></i> Logout</DropdownItem>
                </DropdownMenu>
            </Dropdown>
        );
    } 

    render() {
        return (
            this.dropAccnt()
        );
    }
}

HeaderDropdown.propTypes = {
    logout: Proptypes.func.isRequired,
    history: Proptypes.object.isRequired,
};

export default HeaderDropdown;
