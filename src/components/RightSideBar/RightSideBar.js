import React, {Component} from "react";
import Proptypes from "prop-types";
import {  Card, Col, CardBody, Input, FormGroup, Form, Row, Button } from "reactstrap";
import { getProfile, searchProfile } from "../../actions/actionUser";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class RightSideBar extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentProf : null,
            term: "",
            id: null
        };
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    componentDidMount(){
        this.getProfile();
    }

    getProfile(){
        this.props.getProfile().then((response)=>{
            this.setState({currentProf: response.value.data});
        });
    }

    onInputChange(event) {
        this.setState({ term: event.target.value });
    }

    onFormSubmit() {
        this.props.searchProfile(this.state.term).then((response)=>{
            this.setState({currentProf: response.value.data});
        });
        this.setState({ term:"" });
    }
  
    render(){
        const {currentProf} = this.state;
        return(
            <div className="rightsidebarstyle">
                <Col>
                    {currentProf && currentProf.length > 0 ?
                        <Card>
                            <CardBody>
                                <Form onSubmit={this.onFormSubmit}>
                                    <FormGroup row>
                                        <Col md="9">
                                            <Input type="search" value={this.state.term}
                                                onChange={this.onInputChange} placeholder="Search Connection" />
                                        </Col>
                                        <Col xs="12" md="3">
                                            <Button type="submit" color="primary">Go</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                                <center> <h5 className="connectiontext"> My connections</h5> </center>
                                <div>
                                    <Row>
                                        { currentProf.map((data, index) =>
                                            <Col key={index} className="imgset">
                                                <Link to={{ pathname: `/profiledetails/${data.id}` }}>
                                                    <div className="avatar">
                                                        <img src={data.pic} className="userprofile" alt="Profile" />
                                                        {data.status === "online" ? <span className="avatar-status badge-success"></span> : "" } 
                                                        {data.status === "offline" ? <span className="avatar-status badge-danger"></span> : "" }
                                                        {data.status === "away" ? <span className="avatar-status badge-warning"></span>: "" }
                                                    </div>
                                                    <div className="avtartextcenter">{data.first_name}</div>
                                                </Link>
                                            </Col>
                                        )}
                                    </Row> 
                                </div>
                            </CardBody> 
                        </Card>
                        : " " }
                </Col>
            </div> 
        );
    }
}

RightSideBar.propTypes = {
    getProfile: Proptypes.func,
    searchProfile: Proptypes.func
};

function mapStateToProps(state) {
    return {
        isError: state.user.isError,
        data: state.user.data
    };
}

function mapDispatchToProps(dispatch) {
    return({
        getProfile: () => dispatch(getProfile()),
        searchProfile: (data) => dispatch(searchProfile(data))
    });
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RightSideBar);