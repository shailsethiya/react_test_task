import React, {Component} from "react";
import { connect } from "react-redux";
import { Modal,ModalHeader,Button,ModalBody, ModalFooter } from "reactstrap";
import Proptypes from "prop-types";
import { logout } from "../../actions/actionLogin";

class Logout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };
        this.toggle = this.toggle.bind(this);
        this.confirmLogout = this.confirmLogout.bind(this);
        this.cancelLogout = this.cancelLogout.bind(this);
    }

    toggle(){
        this.setState({modal: !this.state.modal});
    }

    confirmLogout(){
        this.props.logout();
        this.toggle();
    }

    cancelLogout(){
        this.toggle();
        this.props.history.push("/");
    }

    componentDidMount(){
        this.setState({modal: true});
    }

    render() {
        return (
            <div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} >
                    <ModalHeader toggle={this.toggle}>Logout <i className="fa fa-sign-out" aria-hidden="true"></i>
                    </ModalHeader>
                    <ModalBody>
                        <p> Are you sure you want to Logout of Kasm ?</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.confirmLogout} size="sm" color="primary"> <i className="fa fa-sign-out"></i> Logout</Button>
                        <Button onClick={this.cancelLogout} size="sm" color="basic"><i className="fa fa-ban"></i> Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}

Logout.propTypes = {
    logout: Proptypes.func.isRequired,
    history: Proptypes.object.isRequired,
};

function mapStateToProps() {
    return {
    
    };
}

function mapDispatchToProps(dispatch) {
    return({
        logout: () => {dispatch(logout());}
    });
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Logout);