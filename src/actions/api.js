import axios from "axios";

function makeHeaders(){
    let headers = {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization, access-control-allow-origin",
        "Access-Control-Allow-Credentials": true
    };
    return headers;
}

export default axios.create(
    {
        withCredentials: true,
        baseURL: "http://localhost:3000/",
        crossDomain: true,
        headers: makeHeaders(),
    });
