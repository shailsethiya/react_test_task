import api from "./api";

export function postRegister(data) {
    return {
        type: "REGISTER",
        payload: api.post("users/", data),
    };
}