import api from "./api";

export function loginFunction(login_data) {
    var user_info = {"username": login_data.username, "password": login_data.password};
    window.localStorage.setItem("user_info", JSON.stringify(user_info));
    return {
        type: "LOGIN",
        payload: api.get("users/", login_data),
    };
}

export function logout() {
    return {
        type: "LOGOUT"
    };
}

