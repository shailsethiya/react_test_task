import api from "./api";

export function getProfile(data) {
    return {
        type: "USERS",
        payload: api.get("users/", data),
    };
}

export function updateProfile(data) {
    return {
        type: "USERS",
        payload: api.put("users/"+data.id, data),
    };
}

export function searchProfile(data) {
    return {
        type: "USERS",
        payload: api.get("users?q="+data, data),
    };
}