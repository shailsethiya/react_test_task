import api from "./api";

export function getPosts(post) {
    return {
        type: "POST",
        payload: api.get("posts/", post),
    };
}

export function searchPosts(data) {
    return {
        type: "POST",
        payload: api.get("posts?q="+data, data),
    };
}

export function createPost(data) {
    return {
        type: "POST",
        payload: api.post("posts/", data),
    };
}


export function updatePost(data) {
    return {
        type: "POST",
        payload: api.put("posts/"+data.id, data),
    };
}


export function deletePost(data) {
    return {
        type: "POST",
        payload: api.delete("posts/"+data, data),
    };
}