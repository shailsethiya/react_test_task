import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, Col, FormGroup, Container, Form, InputGroupAddon, InputGroupText, Row } from "reactstrap";
import { renderField, required } from "../../../utils/formValidations.js";
import { Field, reduxForm } from "redux-form";
import { postRegister } from "../../../actions/actionRegistration";
import { withRouter } from "react-router-dom";
import { NotificationManager } from "react-notifications";
import Proptypes from "prop-types";
import { connect } from "react-redux";
import bgregister from "../../../../assets/img/background.jpg";
import FLogin from "../../../components/SocialLogin/FLogin";
import GLogin from "../../../components/SocialLogin/GLogin";
 
const validate = values => {
    const errors = {};
    if (values.password !== values.confirmPassword) {
        errors.confirmPassword = "Confirm Password should match Password";
    }
    return errors;
};

class Register extends Component {
    constructor(props){
        super(props);
        this.state  = {
            currentUser: null
        };
        this.submitUser = this.submitUser.bind(this);
    }

    handleCreateSuccess(){
        NotificationManager.success("User Registered Successfully", "Register Successfully");
        this.props.history.push("/login");
    }

    handleCreateError(){
        const {isError} = this.props; 
        if(isError){
            NotificationManager.error(isError,"Registered User", 3000);
        }
    }

    submitUser(userData){ 
        this.props.postRegister(userData).
            then(() => this.handleCreateSuccess()).
            catch(() => this.handleCreateError());
    }
        
    render() {
        const {handleSubmit} = this.props;
        return (
            <div>
                <img src={bgregister} className="bglogin" alt="bgregister" />
                <div className="app flex-row align-items-center">
                    <Container>
                        <Row className="justify-content-center">
                            <Col md="6">
                                <Card className="mx-4">
                                    <Form onSubmit={handleSubmit(this.submitUser)}>
                                        <CardBody className="p-4">
                                            <h1>Register</h1>
                                            <p className="text-muted">Create your account</p>  
                                            <FormGroup>
                                                <Row>
                                                    <Col sm="1">
                                                        <InputGroupAddon addonType="prepend" className="register_form_field">
                                                            <InputGroupText>
                                                                <i className="icon-user"></i>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                    </Col>
                                                    <Col sm="11">
                                                        <Field type="text"
                                                            name="first_name" 
                                                            id="first_name" 
                                                            component={renderField} 
                                                            validate={required}
                                                            placeholder="First Name"
                                                        />
                                                    </Col>
                                                </Row>          
                                            </FormGroup>
                                            <FormGroup>
                                                <Row>
                                                    <Col sm="1">
                                                        <InputGroupAddon addonType="prepend" className="register_form_field">
                                                            <InputGroupText>
                                                                <i className="icon-user"></i>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                    </Col>
                                                    <Col sm="11">
                                                        <Field type="text"
                                                            name="last_name" 
                                                            id="last_name" 
                                                            component={renderField} 
                                                            validate={required}
                                                            placeholder="Last Name"
                                                        />
                                                    </Col>
                                                </Row>          
                                            </FormGroup>
                                            <FormGroup>
                                                <Row>
                                                    <Col sm="1">
                                                        <InputGroupAddon addonType="prepend" className="register_form_field">
                                                            <InputGroupText>
                                                                <i className="icon-phone"></i>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                    </Col>
                                                    <Col sm="11">
                                                        <Field type="number"
                                                            name="contact" 
                                                            id="contact" 
                                                            component={renderField} 
                                                            validate={required}
                                                            placeholder="Contact No."
                                                        />
                                                    </Col>
                                                </Row>          
                                            </FormGroup>
                                            <FormGroup>
                                                <Row>
                                                    <Col sm="1">
                                                        <InputGroupAddon addonType="prepend" className="register_form_field">
                                                            <InputGroupText>
                                                                <i className="fa fa-envelope-o"></i>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                    </Col>
                                                    <Col sm="11">
                                                        <Field type="email"
                                                            name="username" 
                                                            id="username" 
                                                            component={renderField} 
                                                            validate={required}
                                                            placeholder="Email"
                                                        />
                                                    </Col>
                                                </Row>      
                                            </FormGroup>
                                            <FormGroup>
                                                <Row>
                                                    <Col sm="1">
                                                        <InputGroupAddon addonType="prepend" className="register_form_field">
                                                            <InputGroupText>
                                                                <i className="icon-lock"></i>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                    </Col>
                                                    <Col sm="11">
                                                        <Field type="password"
                                                            name="password" 
                                                            id="password" 
                                                            component={renderField} 
                                                            validate={required}
                                                            placeholder="Password"
                                                        />  
                                                    </Col>
                                                </Row>   
                                            </FormGroup>
                                            <FormGroup>
                                                <Row>
                                                    <Col sm="1">
                                                        <InputGroupAddon addonType="prepend" className="register_form_field">
                                                            <InputGroupText>
                                                                <i className="icon-lock"></i>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                    </Col>
                                                    <Col sm="11">
                                                        <Field type="password"
                                                            name="confirmPassword" 
                                                            id="confirmPassword" 
                                                            component={renderField} 
                                                            validate={required}
                                                            placeholder="Confirm Password"
                                                        />
                                                    </Col>
                                                </Row>     
                                            </FormGroup>
                                        </CardBody>
                                        <CardFooter>
                                            <FormGroup className="form-actions">
                                                <Row>
                                                    <Col xs="12" sm="6">
                                                        <Button type="submit" color="success" block> <i className="fa fa-plus-square"></i> Create Account</Button>
                                                    </Col>   
                                                    <Col xs="12" sm="6">
                                                        <Button  color="primary" href="#" to="/login" block><i className="fa fa-sign-in"></i> Sign In</Button>
                                                    </Col>
                                                </Row>
                                            </FormGroup>
                                            <Row>
                                                <Col xs="12" sm="6">
                                                    <GLogin />
                                                </Col>
                                                <Col xs="12" sm="6">
                                                    <FLogin />
                                                </Col>
                                            </Row>
                                        </CardFooter>
                                    </Form>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

Register.propTypes = {
    history: Proptypes.object,
    isError: Proptypes.string,
    initialize: Proptypes.func,
    postRegister: Proptypes.func,
    currentUser: Proptypes.func,
    Id: Proptypes.string,
    handleSubmit: Proptypes.func,
    className: Proptypes.object,
};

let registerWithRouter = withRouter(Register);

let registerForm =  connect(state => 
    ({
        isError: state.register.isError,
        isLoading: state.register.isLoading 
    }),
dispatch => 
    ({
        postRegister: (userData) => dispatch(postRegister(userData))
    }))(registerWithRouter);

export default reduxForm({
    form: "registerForm",
    validate
})(registerForm);