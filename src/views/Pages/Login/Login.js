import React, { Component } from "react";
import { Container, Row, Col, CardGroup, Card, CardBody, Button, InputGroup } from "reactstrap";
import { Field, reduxForm } from "redux-form";
import { loginFunction } from "../../../actions/actionLogin";
import { connect } from "react-redux";
import { required, renderField } from "../../../utils/formValidations.js";
import $ from "jquery";
import { withRouter } from "react-router-dom";
import { NotificationManager } from "react-notifications";
import { Link } from "react-router-dom";
import "../../../../assets/style/style.css";
import bglogin from "../../../../assets/img/login.jpg";
import Proptypes from "prop-types";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: []
        };
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleCreateSuccess(){ 
        //NotificationManager.success("User login Successfully", "Login Successfully");
        this.props.history.push("/dashboard");
    }

    handleCreateError(){
        const {isError} = this.props; 
        if(isError){
            NotificationManager.error(isError,"Post", 3000);
        }
    }

    handleLogin({ username, password }) {
        let login_data = {
            "username": username,
            "password": password
        };
        this.props.loginFunction(login_data).
            then(() => this.handleCreateSuccess()).
            catch(() => this.handleCreateError());
    }

    componentDidMount() {
        if (window.localStorage.getItem("user_info") == null) {
            $("body").addClass("login-body");
        }
        else {
            $("body").removeClass("login-body");
        }
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <div>
                <img src={bglogin} className="bglogin" alt="bglogin" />
                <div className="login_box">
                    <Container>
                        <Row className="justify-content-center testMe">
                            <Col md="8">
                                <CardGroup className="mb-0">
                                    <Card className="p-4">
                                        <CardBody className="card-body col-md-offset-3">
                                            <form onSubmit={handleSubmit(this.handleLogin)}>
                                                <center> <h1>Login</h1> </center>
                                                <p className="text-muted">Sign In to your account
                                                    <span className="error_msg_show">{!this.props.isError ? this.props.isError : " "}</span>
                                                </p>
                                                <InputGroup className="mb-3 login_form_field">
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text">
                                                            <i className="icon-user"></i>
                                                        </span>
                                                    </div>
                                                    <Field
                                                        name="username"
                                                        placeholder="Username"
                                                        component={renderField}
                                                        type="username"
                                                        validate={required}
                                                    />
                                                </InputGroup>
                                                <InputGroup className="mb-4 login_form_field">
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text">
                                                            <i className="icon-lock"></i>
                                                        </span>
                                                    </div>
                                                    <Field 
                                                        name="password"
                                                        placeholder="Password"
                                                        component={renderField}
                                                        type="password"
                                                        validate={required} 
                                                    />
                                                </InputGroup>
                                                <Row>
                                                    <Col xs="12">
                                                        <center> <Button color="primary" className="px-4" style={{"borderRadius" : "3px"}} type="submit">Login</Button> </center>
                                                    </Col>
                                                </Row>
                                            </form>
                                        </CardBody>
                                    </Card>
                                    <Card className="text-white py-5 d-md-down-none" style={{ width: 45 + "%" , "backgroundColor" : "#214e70"}}>
                                        <CardBody className="text-center">
                                            <div>
                                                <h2>Sign up</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                            labore et dolore magna aliqua.</p>
                                                <Link  href="#" to="/register" > <Button color="primary" className="mt-3">Register Now! </Button> </Link>   
                                            </div> 
                                        </CardBody>
                                    </Card>
                                </CardGroup>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    loginFunction: Proptypes.func.isRequired,
    fields: Proptypes.array,
    handleSubmit: Proptypes.func,
    history: Proptypes.object,
    isLoading: Proptypes.bool,
    isError: Proptypes.bool,
    user: Proptypes.array
};

let LoginWithRouter = withRouter(Login);

let LoginForm =  connect(state => 
    ({
        user: state.auth.user,
        isError: state.auth.isError,
        isLoading: state.auth.isLoading
    }),
dispatch => 
    ({
        loginFunction: (login_data) => dispatch(loginFunction(login_data))
    }))(LoginWithRouter);

export default reduxForm({
    form: "LoginPageForm"
})(LoginForm);