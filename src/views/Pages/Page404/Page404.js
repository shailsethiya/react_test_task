import React, {Component} from "react";
import {Container, Row, Col} from "reactstrap";
import { Link } from "react-router-dom";

class Page404 extends Component {
    render() {
        return (
            <div>
                <Link to="/" style= {{align: "left"}}> Go to Home </Link>
                <div className="app flex-row align-items-center">
                    <Container>
                        <Row className="justify-content-center">
                            <Col md="6">
                                <div className="clearfix">
                                    <h1 className="float-left display-3 mr-4">404</h1>
                                    <h4 className="pt-3">Oops! You&apos;re lost.</h4>
                                    <p className="text-muted float-left">The page you are looking for was not found.</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

export default Page404;
