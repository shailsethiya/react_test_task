import React, {Component} from "react";
import {  Button, Card, CardBody, CardFooter, CardHeader, Container, Col, Form, Row, FormGroup, Label } from "reactstrap";import { Field, reduxForm } from "redux-form";
import { updateProfile, getProfile} from "../../actions/actionUser";
import { renderField, required } from "../../utils/formValidations.js";
import { withRouter } from "react-router-dom";
import { NotificationManager } from "react-notifications";
import { connect } from "react-redux";
import Proptypes from "prop-types";

const validate = values => {
    const errors = {};
    if (values.password !== values.confirmPassword) {
        errors.confirmPassword = "Confirm Password should match confirm Password";
    }
    return errors;
};

class ChangePassword extends Component{
    constructor(props){
        super(props);
        this.state  = {
            currentPass: null,
            id: 12
        };
        this.submitPass = this.submitPass.bind(this);
        this.initializeForm = this.initializeForm.bind(this);
    }

    handleCreateSuccess(){ 
        NotificationManager.success("User Password Changed Successfully", "Created Successfully");
        this.props.history.push("/dashboard");
    }

    handleCreateError(){
        const {isError} = this.props; 
        if(isError){
            NotificationManager.error(isError,"Post", 3000);
        }
    }

    initializeForm(res){  
        let currentPass =  res.action.payload.data.find(dat => dat.id == this.state.id);
        this.setState({currentPass: currentPass});
        this.props.initialize({
            password: currentPass.password,
            confirmPassword: currentPass.confirmPassword
        });
    }

    componentDidMount(){
        this.props.getProfile().then((res) => this.initializeForm(res));
    }

    submitPass(userData){
        userData.id = this.state.id;
        this.props.updateProfile(userData).
            then(() => this.handleCreateSuccess()).
            catch(() => this.handleCreateError());
    }

    render(){
        const {handleSubmit} = this.props;
        return(
            <div className="dashboardstyle">
                <Container>
                    <Row className="justify-content-center">
                        <Col>
                            <Card>
                                <Form onSubmit={handleSubmit(this.submitPass)}>
                                    <CardHeader>
                                        <strong>Reset Password</strong>
                                    </CardHeader>
                                    <CardBody>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="password">Password</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="password"
                                                        name="password" 
                                                        id="password" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="Password"
                                                    />  
                                                </Col>     
                                            </Row>
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="confirmPassword"> Confirm Password</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="password"
                                                        name="confirmPassword" 
                                                        id="confirmPassword" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="New Password"
                                                    />  
                                                </Col>     
                                            </Row>
                                        </FormGroup>
                                    </CardBody>
                                    <CardFooter>
                                        <FormGroup className="form-actions">
                                            <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                                            <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
                                        </FormGroup>
                                    </CardFooter>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>  
        );
    }
}

ChangePassword.propTypes ={
    history: Proptypes.object,
    isError: Proptypes.string,
    initialize: Proptypes.func,
    updateProfile: Proptypes.func,
    getProfile: Proptypes.func,
    handleSubmit: Proptypes.func
};

let ChangePasswordWithRouter = withRouter(ChangePassword);

let ChangePasswordForm =  connect(state => 
    ({
        isError: state.register.isError,
        isLoading: state.register.isLoading 
    }),
dispatch => 
    ({
        getProfile: () => dispatch(getProfile()),
        updateProfile: (userData) => dispatch(updateProfile(userData))
    }))(ChangePasswordWithRouter);

export default reduxForm({
    form: "ChangePasswordForm",
    validate
})(ChangePasswordForm);