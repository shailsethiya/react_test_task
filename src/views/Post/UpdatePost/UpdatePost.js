import React, {Component} from "react";
import { Button, Card, CardBody, CardFooter, Container, CardHeader, Col, Form, FormGroup, Row, Label } from "reactstrap";
import { renderField, required, renderTextArea } from "../../../utils/formValidations.js";
import { Field, reduxForm } from "redux-form";
import { updatePost, getPosts } from "../../../actions/actionPost";
import { NotificationManager } from "react-notifications";
import { withRouter } from "react-router-dom";
import Proptypes from "prop-types";
import { connect } from "react-redux";

class UpdatedPost extends Component{
    constructor(props){
        super(props);
        this.state  = {
            currentPosts: [],
            id: null
        };
        this.submitPosts = this.submitPosts.bind(this);
        this.initializeForm = this.initializeForm.bind(this);
    }

    handleUpdateSuccess(){ 
        NotificationManager.success("Updated Post Successfully", "Post Successfully");
        this.props.history.push("/dashboard");
    }

    handleUpdateError(){
        const {isError} = this.props; 
        if(isError){
            NotificationManager.error(isError,"Post", 3000);
        }
    }

    initializeForm(res){
        let currentPost =  res.action.payload.data.find(dat => dat.id == this.props.match.params.id);
        this.setState({currentPosts: currentPost});
        if(currentPost){
            this.props.initialize({
                title: currentPost.title, 
                description: currentPost.description
            });
        }
    }

    componentDidMount(){
        this.props.getPosts().then((res) => this.initializeForm(res));
    }


    submitPosts(userData){
        userData.id = this.state.currentPosts.id;
        this.props.updatePost(userData).
            then(() => this.handleUpdateSuccess()).
            catch(() => this.handleUpdateError());
    }
  
    render(){
        const {handleSubmit} = this.props;
        return(
            <div className="dashboardstyle">
                <Container>
                    <Row className="justify-content-center">
                        <Col>
                            <Card>
                                <Form onSubmit={handleSubmit(this.submitPosts)}>
                                    <CardHeader>
                                        <strong>Update Post</strong>
                                    </CardHeader>
                                    <CardBody>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="title">Post</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="text"
                                                        name="title" 
                                                        id="title" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="Post"
                                                    />  
                                                </Col>     
                                            </Row>
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="description">Description</Label>
                                                </Col>
                                                <Col>
                                                    <Field type="text"
                                                        name="description" 
                                                        id="description" 
                                                        component={renderTextArea} 
                                                        validate={required}
                                                        placeholder="Description"
                                                    />   
                                                </Col>  
                                            </Row>      
                                        </FormGroup>
                                    </CardBody>
                                    <CardFooter>
                                        <FormGroup className="form-actions">
                                            <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                                            <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
                                        </FormGroup>
                                    </CardFooter>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>  
        );
    }
}

UpdatedPost.propTypes = {
    createPost: Proptypes.object,
    postRegister: Proptypes.object,
    handleSubmit: Proptypes.func,
    history: Proptypes.object,
    isError: Proptypes.string,
    match: Proptypes.object,
    initialize: Proptypes.func,
    getPosts: Proptypes.func,
    updatePost: Proptypes.func
};


let UpdatePostWithRouter = withRouter(UpdatedPost);

let UpdatePostForm =  connect(state => 
    ({
        isError: state.register.isError,
        isLoading: state.register.isLoading 
    }),
dispatch => 
    ({
        getPosts: () => dispatch(getPosts()),
        updatePost: (userData) => dispatch(updatePost(userData))
    }))(UpdatePostWithRouter);

export default reduxForm({
    form: "updateForm"
})(UpdatePostForm);