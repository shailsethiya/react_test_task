import React, {Component} from "react";
import {  Button, Card, CardBody, CardFooter, Container, CardHeader, Col, Form, FormGroup, Row, Label } from "reactstrap";
import { renderField, required, renderTextArea } from "../../../utils/formValidations.js";
import { Field, reduxForm } from "redux-form";
import { createPost } from "../../../actions/actionPost";
import { withRouter } from "react-router-dom";
import { NotificationManager } from "react-notifications";
import Proptypes from "prop-types";
import { connect } from "react-redux";


class AddPost extends Component{
    constructor(props){
        super(props);
        this.state  = {
            currentPost: null
        };
        this.submitPost = this.submitPost.bind(this);
    }

    handleCreateSuccess(){ 
        NotificationManager.success("Post Created Successfully", "Post Successfully");
        this.props.history.push("/dashboard");
    }

    handleCreateError(){
        const {isError} = this.props; 
        if(isError){
            NotificationManager.error(isError,"Post", 3000);
        }
    }

    submitPost(userData){
        this.props.createPost(userData).
            then(() => this.handleCreateSuccess()).
            catch(() => this.handleCreateError());
    }
  
    render(){
        const {handleSubmit} = this.props;
        return(
            <div className="dashboardstyle">
                <Container>
                    <Row className="justify-content-center">
                        <Col>
                            <Card>
                                <Form onSubmit={handleSubmit(this.submitPost)}>
                                    <CardHeader>
                                        <strong>Add Post</strong>
                                    </CardHeader>
                                    <CardBody>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="title">Post</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="title"
                                                        name="title" 
                                                        id="title" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="Post"
                                                    />  
                                                </Col>     
                                            </Row>
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="description">Description</Label>
                                                </Col>
                                                <Col>
                                                    <Field type="textarea"
                                                        name="description" 
                                                        id="description" 
                                                        component={renderTextArea} 
                                                        placeholder="Description about your post..."
                                                    />   
                                                </Col>  
                                            </Row>      
                                        </FormGroup>
                                    </CardBody>
                                    <CardFooter>
                                        <FormGroup className="form-actions">
                                            <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                                            <Button size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
                                        </FormGroup>
                                    </CardFooter>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>  
        );
    }
}

AddPost.propTypes = {
    createPost: Proptypes.func,
    history: Proptypes.object,
    isError: Proptypes.string,
    initialize: Proptypes.func,
    handleSubmit: Proptypes.func
};

let AddPostWithRouter = withRouter(AddPost);

let AddPostForm =  connect(state => 
    ({
        isError: state.register.isError,
        isLoading: state.register.isLoading 
    }),
dispatch => 
    ({
        createPost: (userData) => dispatch(createPost(userData))
    }))(AddPostWithRouter);

export default reduxForm({
    form: "postForm"
})(AddPostForm);