import React, {Component} from "react";
import { Card, CardBody, Container, CardHeader, Col, Row, ListGroupItem, ListGroup } from "reactstrap";
import { reduxForm } from "redux-form";
import { getProfile } from "../../actions/actionUser";
import { withRouter } from "react-router-dom";
import Proptypes from "prop-types";
import { connect } from "react-redux";

class ProfileDetails extends Component{
    constructor(props){
        super(props);
        this.state  = {
            currentProfile: [],
            id: null
        };
    }

    componentDidMount(){
        this.getProfile();
    }

    getProfile(){
        this.props.getProfile().then((res)=>{
            let currentProf = res.action.payload.data.find(dat => dat.id == this.props.match.params.id);
            this.setState({currentProfile: currentProf});
        });
    }
  
    render(){
        const {currentProfile} = this.state;
        return(
            <div className="dashboardstyle">
                <Container>
                    <Row className="justify-content-center">
                        {currentProfile ?  
                            <Col>
                                <Card>
                                    <CardHeader>
                                        <strong>Profile Details</strong>
                                    </CardHeader>
                                    <CardBody>
                                        <ListGroup id="list-tab" role="tablist">
                                            <ListGroupItem><span> <img src={currentProfile.pic} className="profileimage" alt="Profile" /></span></ListGroupItem>
                                            <ListGroupItem><span> Name </span> <span style={{"marginLeft": "158px"}}>{currentProfile.first_name} {currentProfile.last_name}</span></ListGroupItem>
                                            <ListGroupItem><span> Email </span> <span style={{"marginLeft": "158px"}}>{currentProfile.username}</span></ListGroupItem>
                                            <ListGroupItem><span> Contact </span> <span style={{"marginLeft" : "145px"}}>{currentProfile.contact}</span></ListGroupItem>
                                        </ListGroup> 
                                    </CardBody>   
                                </Card>
                            </Col>  : " " }
                    </Row>
                </Container>
            </div> 
        );
    }
}

ProfileDetails.propTypes = {
    data: Proptypes.array,
    getProfile: Proptypes.func,
    history: Proptypes.object,
    isError: Proptypes.string,
    match: Proptypes.object
};

let ProfileDetailsWithRouter = withRouter(ProfileDetails);

let ProfileDetailsForm =  connect(state => 
    ({
        isError: state.user.isError,
        isLoading: state.user.isLoading 
    }),
dispatch => 
    ({
        getProfile: () => dispatch(getProfile())
    }))(ProfileDetailsWithRouter);

export default reduxForm({
    form: "ProfileDetailsForm"
})(ProfileDetailsForm);