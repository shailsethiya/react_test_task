import React, {Component} from "react";
import { Row, Col, ListGroup, ListGroupItemHeading, Input, ListGroupItemText, ListGroupItem, Button, Modal, ModalHeader, ModalBody, ModalFooter, 
    Form, FormGroup, UncontrolledTooltip, Card, CardBody} from "reactstrap";
import { NotificationManager } from "react-notifications";
import { getPosts, deletePost, searchPosts } from "../../actions/actionPost";
import { Link } from "react-router-dom";
import { reduxForm } from "redux-form";
import { withRouter } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner/index";
import InfiniteScroll from "react-infinite-scroll-component";
import Proptypes from "prop-types";
import { connect } from "react-redux";

class Dashboard extends Component{
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            currentData: [],
            id: null,
            term: "",
            hasMore: true
        };
        this.deleteConfirm = this.deleteConfirm.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.deletePosts = this.deletePosts.bind(this);
        this.toggle = this.toggle.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.fetchMoreData = this.fetchMoreData.bind(this);
    }

    deleteConfirm(id){
        this.setState({modal: !this.state.modal,
            id: id});
    }

    deletePosts(){
        this.props.deletePost(this.state.id).
            then(() => this.handleDeleteSuccess()).
            catch(() => this.handleDeleteError());
    }

    componentDidMount(){
        this.getPosts(); 
    }
    
    getPosts(){
        this.props.fetchPosts(this.state.id).then((response)=>{
            this.setState({currentData: response.value.data});
        });
    }

    fetchMoreData = () => {
        this.props.fetchPosts(this.state.currentData.length);
    };

    onInputChange(event) {
        this.setState({ term: event.target.value });
    }

    onFormSubmit() {
        this.props.searchPosts(this.state.term).then((response)=>{
            this.setState({currentData: response.value.data});
        });
        this.setState({ term: "" });
    }

    handleDeleteSuccess(){
        NotificationManager.success("Post Deleted Successfully","Delete Post");
        this.props.getPosts().then((response)=>{
            this.setState({currentData: response.value.data, modal: false});
        });
    }

    handleDeleteError(){
        const {isError} = this.props;
        if(isError){
            NotificationManager.error(isError,"Delete Post", 3000);
            this.setState({modal: false});
        }
        else{
            NotificationManager.error("Delete Post Failed","Delete Post", 3000);
            this.setState({modal: false});
        }
    }

    toggle(){
        this.setState({modal: !this.state.modal});
    }
  
    render(){
        if (this.state.isLoading){
            return (<div> <LoadingSpinner /></div>);
        }
        const {currentData} = this.state;
        return(
            <div className="dashboardstyle">
                <Row>
                    <Col>
                        {currentData && currentData.length > 0 ? 
                            <Card> 
                                <CardBody>
                                    <InfiniteScroll
                                        dataLength={this.state.currentData.length}
                                        hasMore={this.state.hasMore}
                                        next={this.fetchMoreData}
                                    >
                                        <Form onSubmit={this.onFormSubmit}>
                                            <FormGroup row className="flexwarpclass">
                                                <Col md="9">
                                                    <Input type="search"  value={this.state.term}
                                                        onChange={this.onInputChange} placeholder="Search any word or phrase" />
                                                </Col>
                                                <Col xs="12" md="3"> 
                                                    <Button type="submit" color="primary">Go</Button>
                                                </Col>
                                            </FormGroup>
                                        </Form>
                                        { currentData.map((data, index) =>
                                            <ListGroup key={index}>
                                                <ListGroupItem>
                                                    <ListGroupItemHeading className="headertext">{data.title}</ListGroupItemHeading>
                                                    <ListGroupItemText className="textfont-dash">{data.description}</ListGroupItemText>
                                                    <Link className="btn btn-warning" id="editpost" to= {{pathname: `/updatepost/${data.id}`}}>
                                                        <i className="fa fa-pencil"></i> </Link>                                                                                     
                                                    <button className="btn btn-danger" href="#" id="deletepost" onClick={() => this.deleteConfirm(data.id)}>
                                                        <i className="fa fa-trash-o"></i> </button>
                                                    <UncontrolledTooltip placement="top" target="editpost">
                                                    Edit
                                                    </UncontrolledTooltip>
                                                    <UncontrolledTooltip placement="top" target="deletepost">
                                                    Delete
                                                    </UncontrolledTooltip>
                                                </ListGroupItem>
                                            </ListGroup>)}
                                    </InfiniteScroll>
                                </CardBody>
                            </Card>
                            : <Card> <CardBody>No Data Found</CardBody> </Card> }
                    </Col>
                </Row>
                <Modal isOpen={this.state.modal} toggle={this.deleteConfirm} className={this.props.className}>
                    <ModalHeader toggle={this.deleteConfirm}>Delete Post <i className="fa fa-bomb" aria-hidden="true"></i>
                    </ModalHeader>
                    <ModalBody>
                      Confirm Delete Post?
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.deletePosts} size="sm" color="danger"> <i className="fa fa-trash"></i> Delete</Button>
                        <Button onClick={this.toggle} size="sm" color="basic"><i className="fa fa-ban"></i> Close</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Dashboard.propTypes = {
    deletePost: Proptypes.func,
    className: Proptypes.object,
    getPosts: Proptypes.func,
    isError: Proptypes.string,
    isLoading: Proptypes.bool,
    searchPosts: Proptypes.func,
    fetchPosts: Proptypes.func
};

let DashboardWithRouter = withRouter(Dashboard);

let DashboardForm =  connect(state => 
    ({
        isError: state.post.isError,
        isLoading: state.post.isLoading || false
    }),
dispatch => 
    ({
        getPosts: () => dispatch(getPosts()),
        searchPosts: (data)=> dispatch(searchPosts(data)),
        deletePost: (data) => dispatch(deletePost(data)),
        fetchPosts: (userId, startLimit = 0, searchTerm = "" ) => dispatch(getPosts(userId, startLimit, searchTerm))
    }))(DashboardWithRouter);

export default reduxForm({
    form: "dashboardForm"
})(DashboardForm);