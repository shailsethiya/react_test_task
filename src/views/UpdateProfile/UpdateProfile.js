import React, {Component} from "react";
import {  Button, Card, CardBody, CardFooter, Container, CardHeader, Col, Form, FormGroup, Row, Label } from "reactstrap";
import { renderField, required } from "../../utils/formValidations.js";
import { Field, reduxForm } from "redux-form";
import { updateProfile, getProfile } from "../../actions/actionUser";
import { withRouter } from "react-router-dom";
import { NotificationManager } from "react-notifications";
import Proptypes from "prop-types";
import { connect } from "react-redux";

const validate = values => {
    const errors = {};
    if (values.password !== values.confirmPassword) {
        errors.confirmPassword = "Confirm Password should match Password";
    }
    return errors;
};

class UpdateProfile extends Component{
    constructor(props){
        super(props);
        this.state  = {
            currentProfile: [],
            id: 12
        };
        this.submitProfile = this.submitProfile.bind(this);
        this.initializeForm = this.initializeForm.bind(this);
    }

    handleUpdateSuccess(){ 
        NotificationManager.success("User Profile Update Successfully", "Update Successfully");
        this.props.history.push("/dashboard");
    }

    handleUpdateError(){
        const {isError} = this.props; 
        if(isError){
            NotificationManager.error(isError, "Update Failed", 3000);
        }
    }

    initializeForm(res){
        let currentProfil = res.action.payload.data.find(dat => dat.id == this.state.id);
        this.setState({currentProfile: currentProfil});
        if(currentProfil){
            this.props.initialize({
                first_name: currentProfil.first_name, 
                last_name: currentProfil.last_name,
                username: currentProfil.username,
                password: currentProfil.password,
                status: currentProfil.status,
                confirmPassword: currentProfil.confirmPassword,
                contact: currentProfil.contact
            });
        }
    }

    componentDidMount(){
        this.props.getProfile().then((res) => this.initializeForm(res));
    }

    submitProfile(userData){
        userData.id = this.state.id;
        this.props.updateProfile(userData).
            then(() => this.handleUpdateSuccess()).
            catch(() => this.handleUpdateError());
    }
  
    render(){
        const {handleSubmit} = this.props;
        return(
            <div className="dashboardstyle">
                <Container>
                    <Row className="justify-content-center">
                        <Col>
                            <Card>
                                <Form onSubmit={handleSubmit(this.submitProfile)}>
                                    <CardHeader>
                                        <strong>Profile Details</strong>
                                    </CardHeader>
                                    <CardBody>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="name">First Name</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="text"
                                                        name="first_name" 
                                                        id="first_name" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="First Name"
                                                    />    
                                                </Col> 
                                            </Row>
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="name">Last Name</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="text"
                                                        name="last_name" 
                                                        id="last_name" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="Last Name"
                                                    />    
                                                </Col> 
                                            </Row>
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="email">Email</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="email"
                                                        name="username" 
                                                        id="username" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="Email"
                                                    />   
                                                </Col> 
                                            </Row> 
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="password">Password</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="password"
                                                        name="password" 
                                                        id="password" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="Password"
                                                    />  
                                                </Col>  
                                            </Row>   
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="password">Confirm Password</Label>
                                                </Col>
                                                <Col md="9">
                                                    <Field type="password"
                                                        name="confirmPassword" 
                                                        id="confirmPassword" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="Confirm Password"
                                                    />  
                                                </Col>  
                                            </Row>   
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="contact">Contact</Label>
                                                </Col>
                                                <Col>
                                                    <Field type="number"
                                                        name="contact" 
                                                        id="contact" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder=""
                                                    />   
                                                </Col>      
                                            </Row>  
                                        </FormGroup>
                                        <FormGroup>
                                            <Row>
                                                <Col md="3">
                                                    <Label htmlFor="status">Status</Label>
                                                </Col>
                                                <Col>
                                                    <Field type="text"
                                                        name="status" 
                                                        id="status" 
                                                        component={renderField} 
                                                        validate={required}
                                                        placeholder="status"
                                                    />   
                                                </Col>      
                                            </Row>  
                                        </FormGroup>
                                    </CardBody>
                                    <CardFooter>
                                        <FormGroup className="form-actions">
                                            <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                                            <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
                                        </FormGroup>
                                    </CardFooter>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div> 
        );
    }
}

UpdateProfile.propTypes = {
    updateProfile: Proptypes.func,
    handleSubmit: Proptypes.func,
    history: Proptypes.object,
    isError: Proptypes.string,
    match: Proptypes.object,
    initialize: Proptypes.func,
    getProfile: Proptypes.func
};

let UpdateProfileWithRouter = withRouter(UpdateProfile);

let UpdateProfileForm =  connect(state => 
    ({
        isError: state.register.isError,
        isLoading: state.register.isLoading 
    }),
dispatch => 
    ({
        getProfile: () => dispatch(getProfile()),
        updateProfile: (userData) => dispatch(updateProfile(userData))
    }))(UpdateProfileWithRouter);

export default reduxForm({
    form: "profileForm",
    validate
})(UpdateProfileForm);